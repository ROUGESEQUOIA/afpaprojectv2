/**
 * The `isLoggedIn` function is a middleware function that checks if a user is authenticated. It takes three parameters: `req` (request), `res` (response), and `next` (next middleware function).
 * 
 * @function
 * @name isLoggedIn
 * @kind variable
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {any}
 */
const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  const errorMessage = "Vous devez être connecté pour accéder à cette page";
  res.render("auth/login", { errorMessage });
};

/**
 * The `isAdmin` function is a middleware function that checks if a user is an administrator. It takes three parameters: `req` (request), `res` (response), and `next` (next middleware function).
 * 
 * @function
 * @name isAdmin
 * @kind variable
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {any}
 */
const isAdmin = (req, res, next) => {
  if (req.user && req.user.role <= 2) {
    return next();
  }
  const errorMessage = "Vous devez être administrateur pour accéder à cette page";
  res.render("auth/show", { errorMessage });
};

/**
 * The `isNotGuest` function is a middleware function that checks if a user is not a guest. It takes three parameters: `req` (request), `res` (response), and `next` (next middleware function).
 * 
 * @function
 * @name isNotGuest
 * @kind variable
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {any}
 */
const isNotGuest = (req, res, next) => {
  if (req.user && req.user.role !== 4) {
    return next();
  }
  const errorMessage = "Vous devez avoir les privilèges requis pour accéder à cette page";
  res.render("auth/show", { errorMessage });
};

module.exports = {
  isLoggedIn,
  isAdmin,
  isNotGuest,
};
