/**
 * The code is defining a function named `clearMessages` that takes three parameters: `req`, `res`, and `next`. This function is used as middleware in an Express.js application. It checks if there are any error or success messages stored in the session object (`req.session`). If there is an error message, it assigns it to the `res.locals.errorMessage` variable and deletes it from the session. If there is a success message, it assigns it to the `res.locals.successMessage` variable and deletes it from the session. Finally, it calls the `next` function to pass control to the next middleware or route handler.
 * 
 * @function
 * @name clearMessages
 * @kind variable
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {void}
 */
const clearMessages = (req, res, next) => {
    if (req.session.errorMessage) {
      res.locals.errorMessage = req.session.errorMessage;
      delete req.session.errorMessage;
    }
  
    if (req.session.successMessage) {
      res.locals.successMessage = req.session.successMessage;
      delete req.session.successMessage;
    }
  
    next();
  };
  
  module.exports = clearMessages;