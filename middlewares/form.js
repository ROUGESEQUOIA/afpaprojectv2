/**
 * The `const renderFormPage = async (` is declaring a variable named `renderFormPage` and assigning it an asynchronous function. This function takes in several parameters: `res` (response object), `currObject` (current object), `formType` (string indicating the type of form), `hasError` (boolean indicating if there is an error), and `optModel` (optional model object).
 * 
 * @async
 * @function
 * @name renderFormPage
 * @kind variable
 * @param {any} res
 * @param {any} currObject
 * @param {any} formType
 * @param {boolean} hasError?
 * @param {null} optModel?
 * @returns {Promise<void>}
 */
const renderFormPage = async (
  res,
  currObject,
  formType,
  hasError = false,
  optModel = null
) => {
  try {
    const optObjects = optModel ? await optModel.find({}) : [];

    const params = {
      items: optObjects,
      item: currObject,
    };

    const viewName = `${currObject.constructor.modelName.toLowerCase()}s/${formType}`;

    if (hasError) {
      if (formType === "edit") {
        params.errorMessage = `La création de la ${currObject.constructor.modelName.toLowerCase()} a échouée.`;
      } else {
        params.errorMessage = `La modification de la ${currObject.constructor.modelName.toLowerCase()} a échouée`;
      }
    }

    res.render(viewName, params);
  } catch {
    res.redirect("/");
  }
};

/**
 * The `const renderNewPage = async (` is declaring a variable named `renderNewPage` and assigning it an asynchronous function. This function takes in several parameters: `res` (response object), `currObject` (current object), `hasError` (boolean indicating if there is an error), and `optModel` (optional model object). This function is responsible for rendering the new page for a specific object. It calls the `renderFormPage` function passing the appropriate parameters for the new form. If there is an error rendering the new page, it will redirect to the home page.
 * 
 * @async
 * @function
 * @name renderNewPage
 * @kind variable
 * @param {any} res
 * @param {any} currObject
 * @param {any} hasError
 * @param {null} optModel?
 * @returns {Promise<void>}
 */
const renderNewPage = async (
  res,
  currObject,
  hasError,
  optModel = null,
) => {
  try {
    await renderFormPage(res, currObject, "new", hasError, optModel);
  } catch {
    res.redirect("/");
  }
};

/**
 * The `const renderEditPage` is a variable that is assigned an asynchronous function. It takes in several parameters: `res` (response object), `currObject` (current object), `hasError` (boolean indicating if there is an error), and `optModel` (optional model object). This function is responsible for rendering the edit page for a specific object. It calls the `renderFormPage` function passing the appropriate parameters for the edit form. If there is an error, it will display an error message. If there is an error rendering the edit page, it will redirect to the home page.
 * 
 * @async
 * @function
 * @name renderEditPage
 * @kind variable
 * @param {any} res
 * @param {any} currObject
 * @param {any} hasError
 * @param {null} optModel?
 * @returns {Promise<void>}
 */
const renderEditPage = async (
  res,
  currObject,
  hasError,
  optModel = null,
) => {
  try {
    await renderFormPage(res, currObject, "edit", hasError, optModel);
  } catch {
    res.redirect(`/`);
  }
};

module.exports = { renderNewPage, renderEditPage };
