/**
 * The line `let darkMode = localStorage.getItem('darkMode');` is retrieving the value of the 'darkMode' key from the localStorage. It assigns the retrieved value to the variable `darkMode`.
 * 
 * @let
 * @name darkMode
 * @kind variable
 * @type {string | null}
 */
let darkMode = localStorage.getItem('darkMode'); 

const darkModeToggle = document.querySelector('#dark-mode-toggle');

/**
 * The `const enableDarkMode = () => {` is defining a function named `enableDarkMode`. This function adds the class 'darkmode' to the body element of the document, indicating that the dark mode is enabled. It also updates the 'darkMode' value in the localStorage to 'enabled'.
 * 
 * @function
 * @name enableDarkMode
 * @kind variable
 * @returns {void}
 */
const enableDarkMode = () => {
  // 1. Add the class to the body
  document.body.classList.add('darkmode');
  // 2. Update darkMode in localStorage
  localStorage.setItem('darkMode', 'enabled');
}

/**
 * The `const disableDarkMode = () => {` is defining a function named `disableDarkMode`. This function removes the class 'darkmode' from the body element of the document, indicating that the dark mode is disabled. It also updates the 'darkMode' value in the localStorage to null.
 * 
 * @function
 * @name disableDarkMode
 * @kind variable
 * @returns {void}
 */
const disableDarkMode = () => {
  // 1. Remove the class from the body
  document.body.classList.remove('darkmode');
  // 2. Update darkMode in localStorage 
  localStorage.setItem('darkMode', null);
}
 
if (darkMode === 'enabled') {
  enableDarkMode();
}

/**
 * `darkModeToggle.addEventListener('click', () => {` is adding an event listener to the `darkModeToggle` element. It listens for a click event on the element and executes the callback function when the click event occurs.
 * 
 * @constant
 * @name darkModeToggle
 * @type {Element | null}
 */
darkModeToggle.addEventListener('click', () => {
  // get their darkMode setting
  darkMode = localStorage.getItem('darkMode'); 
  
  // if it not current enabled, enable it
  if (darkMode !== 'enabled') {
    enableDarkMode();
  // if it has been enabled, turn it off  
  } else {  
    disableDarkMode(); 
  }
});