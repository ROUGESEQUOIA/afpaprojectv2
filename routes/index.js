// Imports

const express = require("express");
const indexController = require("../controllers/indexController");

// Config

const router = express.Router(); // stockage des fonctionnalités router d’express dans une variable

// Route

router.get('/', indexController.homepage);

// Export du router pour utilisation dans server.js

module.exports = router;
