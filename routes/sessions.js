// Imports

const express = require("express");
const path = require("path"); // librairie pour gérer les path ;-) // Fait partie de Node
const multer = require("multer"); // permet de gérer le stockage des images

const Session = require("../models/session");
const sessionsController = require("../controllers/sessionsController");

// Middlewares utilitaires

const { isLoggedIn, isNotGuest } = require("../middlewares/auth");

// Import gestion des fichiers (images)

const uploadPath = path.join("public", Session.flyerImageBasePath);
const imageMimeTypes = ["image/jpeg", "image/png", "image/gif"];

// Config

const router = express.Router(); // stockage des fonctionnalités router d’express dans une variable

const upload = multer({
  dest: uploadPath,
  fileFilter: (req, file, callback) => {
    callback(null, imageMimeTypes.includes(file.mimetype));
  },
});

// Route : liste des sessions

router.get("/", sessionsController.sessionsList);

// Route : ajout d’une session

router.get("/new", isLoggedIn, isNotGuest, sessionsController.sessionCForm);

// Route : création d’une session

router.post("/", isLoggedIn, isNotGuest, upload.single("image"), sessionsController.createSession);

// Route affichage d’une session

router.get("/:id", sessionsController.sessionShow);

// Route : édition d’une session

router.get("/:id/edit", isLoggedIn, isNotGuest, sessionsController.sessionEForm);

// Route : modification d’une session

router.put("/:id", isLoggedIn, isNotGuest, upload.single("image"), sessionsController.updateSession);

// Route : suppression d’une session

router.delete("/:id", isLoggedIn, isNotGuest, sessionsController.deleteSession);

module.exports = router;
