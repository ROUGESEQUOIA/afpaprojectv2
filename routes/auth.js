// Imports

const express = require("express");

const authController = require("../controllers/authController");

// Config

const router = express.Router(); // stockage des fonctionnalités router d’express dans une variable

// Route : formulaire d’enregistrement

router.get("/register", authController.registrationForm);

// Route : enregistrement d’un utilisateur dans la bdd

router.post("/register", authController.registration);

// Route : formulaire de login

router.get("/login", authController.loginForm);

// Route : connexion

router.post("/login", authController.userConnection);

// Route : déconnexion

router.get("/logout", authController.userDisconnection);

// Route : affichage du profil de l'utilisateur connecté

router.get("/show/:id", authController.userProfil);

// Export

module.exports = router;
