// Imports

const express = require("express");

const formationsRouter = require("../controllers/formationsController");

const { isLoggedIn, isNotGuest } = require("../middlewares/auth");

// Config

const router = express.Router(); // stockage des fonctionnalités router d’express dans une variable

// Route : liste des formations

router.get("/", formationsRouter.formList);

// Route : ajout d’une formation

router.get("/new", isLoggedIn, isNotGuest, formationsRouter.formCForm);

// Route : création d’une formation

router.post("/", isLoggedIn, isNotGuest, formationsRouter.createForm);

// Route : affichage d’une formation

router.get("/:id", formationsRouter.formShow);

// Route : formulaire de modification d’une formation

router.get("/:id/edit", isLoggedIn, isNotGuest, formationsRouter.formEForm);

// Route : modification d’une formation

router.put("/:id", isLoggedIn, isNotGuest, formationsRouter.updateForm);

// Route : suppression d’une formation

router.delete("/:id", isLoggedIn, isNotGuest, formationsRouter.deleteForm);

// Export du router pour utilisation dans server.js

module.exports = router;
