const express = require("express");

const adminController = require("../controllers/adminController");

const { isLoggedIn, isAdmin } = require("../middlewares/auth");

const router = express.Router();

router.get("/", isLoggedIn, isAdmin, adminController.usersList);

router.get("/:id/edit", isLoggedIn, isAdmin, adminController.userEForm);

router.put("/:id/edit", isLoggedIn, isAdmin, adminController.updateUser);

router.delete("/:id/delete", isLoggedIn, isAdmin, adminController.deleteUser);

module.exports = router;