// Imports

const Formation = require("../models/formation");
const Session = require("../models/session");

const { renderNewPage, renderEditPage } = require("../middlewares/form");

// Liste des formations

/**
 * The `exports.formList` function is a controller function that handles the logic for displaying a list of formations. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @async
 * @property
 * @name formList
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.formList = async (req, res) => {
  let searchO = {};

  if (req.query.title != null && req.query.title !== "") {
    // query car dans l’url (GET dans le formulaire)
    searchO.title = new RegExp(req.query.title, "i");
  }

  try {
    const formations = await Formation.find(searchO);

    res.render("formations/index", {
      formations: formations,
      searchO: req.query,
    });
  } catch {
    res.status(500).redirect("/");
  }
};

// Ajout d’une formation

/**
 * The `exports.formCForm` function is a controller function that handles the logic for rendering the form to create a new formation. It takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @property
 * @name formCForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {void}
 */
exports.formCForm = (req, res) => {
  renderNewPage(res, new Formation(), false);
};

// Création d’une formation

/**
 * The `exports.createForm` function is a controller function that handles the logic for creating a new formation. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @async
 * @property
 * @name createForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.createForm = async (req, res) => {
  const {
    title,
    actionType,
    validationType,
    centerTime,
    companyTime,
    centerId,
    description,
  } = req.body;

  const formation = new Formation({
    title,
    actionType,
    validationType,
    centerTime,
    companyTime,
    center_id: centerId,
    description,
  });

  try {
    const newFormation = await formation.save();
    res.status(201).redirect(`formations/${newFormation.id}`);
  } catch (error) {
    renderNewPage(res, formation, true);
  }
};

// Affichage d’une formation

/**
 * The `exports.formShow` function is a controller function that handles the logic for displaying a specific formation. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @async
 * @property
 * @name formShow
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.formShow = async (req, res) => {
  try {
    const formation = await Formation.findById(req.params.id);
    const sessions = await Session.find({ formation: formation.id })
      .limit(10)
      .exec();

    res.render("formations/show", {
      formation: formation,
      sessionsByFormation: sessions,
    });
  } catch {
    res.status(404).redirect("/");
  }
};

// Formulaire de modification d’une formation

/**
 * The `exports.formEForm` function is a controller function that handles the logic for rendering the form to edit a specific formation. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @async
 * @property
 * @name formEForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.formEForm = async (req, res) => {
  try {
    const formation = await Formation.findById(req.params.id);
    renderEditPage(res, formation, false);
  } catch {
    res.redirect("/formations");
  }
};

// Modification d’une formation

/**
 * The `exports.updateForm` function is a controller function that handles the logic for updating a specific formation. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters. It updates the formation with the new values provided in the request body and redirects to the updated formation's page. If there is an error during the update process, it renders the edit page again with an error message.
 * 
 * @async
 * @property
 * @name updateForm
 * @kind function
 * @type {function}
 * @param {{ body: { title: any actionType: any validationType: any centerTime: any companyTime: any centerId: any description: any } params: { id: any } }} { body: { title, actionType, validationType, centerTime, companyTime, centerId, description, }, params: { id }, }
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.updateForm = async (
  {
    body: {
      title,
      actionType,
      validationType,
      centerTime,
      companyTime,
      centerId,
      description,
    },
    params: { id },
  },
  res
) => {
  try {
    const formation = await Formation.findById(id);

    formation.set({
      title,
      actionType,
      validationType,
      centerTime,
      companyTime,
      center_id: centerId,
      description,
    });

    await formation.save();
    res.status(200).redirect(`/formations/${formation.id}`);
  } catch {
    res.renderEditPage(res, formation, true);
  }
};

// Route : suppression d’une formation

/**
 * The `exports.deleteForm` function is a controller function that handles the logic for deleting a specific formation. It is an asynchronous function that takes in the `req` (request) and `res` (response) objects as parameters.
 * 
 * @async
 * @property
 * @name deleteForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.deleteForm = async (req, res) => {
  try {
    const formation = await Formation.findById(req.params.id);
    await formation.deleteOne();
    res.status(204).redirect("/formations");
  } catch (error) {
    if (error.name === "FormationDeleteError") {
      req.session.errorMessage =
        "Impossible de supprimer une formation contenant des sessions";
      res.status(400).redirect(`/formations/${req.params.id}`);
    } else if (formation == null) {
      req.session.errorMessage = "La formation n’existe pas";
      res.status(404).redirect("/");
    } else {
      req.session.errorMessage = "Erreur au cours de la suppression";
      res.status(500).redirect(`/formations/${formation.id}`);
    }
  }
};
