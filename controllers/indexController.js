// Imports

const Session = require("../models/session");

// Routes

/**
 * `exports.homepage = async (req, res) => {` is exporting a function named `homepage` that takes in two parameters `req` and `res`. This function is an asynchronous function, indicated by the `async` keyword. It is used as a route handler for the homepage route.
 * 
 * @async
 * @property
 * @name homepage
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.homepage = async (req, res) => {
  let sessions;
  try {
    sessions = await Session.find()
      .sort({ createdAt: "desc" })
      .limit(15)
      .exec();
  } catch {
    sessions = [];
  }
  res.render("index", { sessions: sessions });
}

