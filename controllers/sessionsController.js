// Imports

const path = require("path"); // librairie pour gérer les path ;-) // Fait partie de Node

const fs = require("fs"); // File system, fait partie de Node

const Formation = require("../models/formation");
const Session = require("../models/session");

const uploadPath = path.join("public", Session.flyerImageBasePath);

// Middlewares utilitaires

const { renderNewPage, renderEditPage } = require("../middlewares/form");

// Liste des sessions

/**
 * The `exports.sessionsList` function is a controller function that handles the logic for retrieving a list of sessions. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name sessionsList
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.sessionsList = async (req, res) => {
  let query = Session.find();

  if (req.query.title != null && req.query.title != "") {
    query = query.regex("title", new RegExp(req.query.title, "i")); // -> session.title = req.query.title || paramètre "i" pour insensible à la casse
  }

  if (req.query.sessionStart != null && req.query.sessionStart != "") {
    query = query.gte("sessionStart", req.query.sessionStart);
  }

  if (req.query.sessionEnd != null && req.query.sessionEnd != "") {
    query = query.lte("sessionEnd", req.query.sessionEnd);
  }

  try {
    const sessions = await query.exec(); // exec() permet l’execution de la query définie au-dessus

    res.render("sessions/index", {
      sessions: sessions,
      searchO: req.query,
    });
  } catch {
    res.redirect("/");
  }
};

// Formulaire de création d’une session

/**
 * The `exports.sessionCForm` function is a controller function that handles the logic for rendering the form for creating a new session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name sessionCForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.sessionCForm = async (req, res) => {
  renderNewPage(res, new Session(), false, Formation);
};

// Création d’une session

/**
 * The `exports.createSession` function is a controller function that handles the logic for creating a new session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name createSession
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.createSession = async (req, res) => {
  const {
    title,
    formation,
    actionNumber,
    sessionStart,
    sessionEnd,
    description,
  } = req.body;

  const fileName = req.file?.filename;

  const session = new Session({
    title,
    formation,
    actionNumber,
    sessionStart: new Date(sessionStart),
    sessionEnd: new Date(sessionEnd),
    flyer: fileName,
    description,
  });

  try {
    const newSession = await session.save();
    res.redirect(`sessions/${newSession.id}`);
  } catch {
    if (session.flyer) {
      removeSessionFile(session.flyer);
    }
    renderNewPage(res, session, true, Formation);
  }
};

// Affichage d’une session

/**
 * The `exports.sessionShow` function is a controller function that handles the logic for displaying a specific session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name sessionShow
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.sessionShow = async (req, res) => {
  try {
    const session = await Session.findById(req.params.id)
      .populate("formation") // permet de récupérer la formation qui lui est liée
      .exec();

    res.render("sessions/show", { session: session });
  } catch {
    res.redirect("/");
  }
};

// Formulaire de modification d’une session

/**
 * The `exports.sessionEForm` function is a controller function that handles the logic for rendering the form for editing a session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name sessionEForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.sessionEForm = async (req, res) => {
  try {
    const session = await Session.findById(req.params.id);

    renderEditPage(res, session, false, Formation);
  } catch {
    res.redirect("/");
  }
};

// Modification d’une session

/**
 * The `exports.updateSession` function is a controller function that handles the logic for updating a session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name updateSession
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.updateSession = async (req, res) => {
  const {
    title,
    formation,
    actionNumber,
    sessionStart,
    sessionEnd,
    description,
  } = req.body;

  const fileName = req.file?.filename;
  /* const fileName = req.file != null ? req.file.filename : null; */

  let session;

  try {
    session = await Session.findById(req.params.id);

    session.title = title;
    session.formation = formation;
    session.actionNumber = actionNumber;
    session.sessionStart = new Date(sessionStart);
    session.sessionEnd = new Date(sessionEnd);

    if (fileName != null) {
      removeSessionFile(session.flyer);
      session.flyer = fileName;
    } else {
      session.flyer = session.flyer;
    }

    /* session.flyer = fileName ?? session.flyer; */

    session.description = description;

    await session.save();

    res.redirect(`/sessions/${session.id}`);
  } catch {
    if (session) {
      renderEditPage(res, session, true, Formation);
    } else {
      redirect("/");
    }
  }
};

// Suppression d’une session

/**
 * The `exports.deleteSession` function is a controller function that handles the logic for deleting a session. It is an asynchronous function that takes in two parameters: `req` (the request object) and `res` (the response object).
 * 
 * @async
 * @property
 * @name deleteSession
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.deleteSession = async (req, res) => {
  let session;

  try {
    session = await Session.findById(req.params.id);
    await session.deleteOne();

    if (session.flyer) {
      removeSessionFile(session.flyer);
    }

    res.redirect("/sessions");
  } catch (err) {
    console.log(err);
    if (session != null) {
      res.render("sessions/show", {
        session: session,
        errorMessage: "Impossible de supprimer cette session",
      });
    } else {
      res.redirect("/");
    }
  }
};

// Suppression de l’image d’une session

/**
 * The `removeSessionFile` function is a utility function that is used to delete a session's image file from the file system. It takes in a `fileName` parameter, which represents the name of the image file to be deleted.
 * 
 * @function
 * @name removeSessionFile
 * @kind function
 * @param {any} fileName
 * @returns {void}
 */
function removeSessionFile(fileName) {
  fs.unlink(path.join(uploadPath, fileName), (err) => {
    if (err) console.log(err); // Pas pertinent pour les utilisateurs donc dans la console
  });
}
