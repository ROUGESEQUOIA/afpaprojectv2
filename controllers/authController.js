// Imports

const passport = require("passport");

const User = require("../models/user");

// Config passport local strategy : utilisation du model User

passport.use(User.createStrategy());

// Serialize et deserialize

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

/* passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
}); */

passport.deserializeUser(function (id, done) {
  User.findById(id)
    .exec()
    .then((user) => {
      done(null, user);
    })
    .catch((err) => {
      done(err);
    });
});


// Formulaire d’enregistrement

/**
 * The `exports.registrationForm` function is responsible for rendering the registration form view. It takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework. Inside the function, it calls the `res.render` method to render the "auth/register" view, which will display the registration form to the user.
 * 
 * @property
 * @name registrationForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {void}
 */
exports.registrationForm = (req, res) => {
  res.render("auth/register");
};

// Enregistrement d’un utilisateur dans la bdd

/**
 * The `exports.registration` function is responsible for handling the registration process of a user. It is an asynchronous function that takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework.
 * 
 * @async
 * @property
 * @name registration
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.registration = async (req, res) => {
  try {
    // Enregistrement

    const registerUser = await User.register(
      { username: req.body.username, role: 4 },
      req.body.password
    ); // register() est utilisable grâce à l’utilisation de passport local sur le model userSchema
 
    if (registerUser) {
      passport.authenticate("local")(req, res, function () {
        res.redirect("/");
      });
    } else {
      req.session.errorMessage = "Erreur lors de l’inscription";
      res.redirect("/auth/register");
    }
  } catch (error) {
    console.log(error);
    req.session.errorMessage = "Nom d'utilisateur non autorisé";
    res.redirect("/auth/register");
  }
};

// Formulaire de login

/**
 * The `exports.loginForm` function is responsible for rendering the login form view. It takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework. Inside the function, it calls the `res.render` method to render the "auth/login" view, which will display the login form to the user.
 * 
 * @property
 * @name loginForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {void}
 */
exports.loginForm = (req, res) => {
  res.render("auth/login");
};

// Connexion

/**
 * The `exports.userConnection` function is responsible for handling the login process of a user. It is an asynchronous function that takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework.
 * 
 * @async
 * @property
 * @name userConnection
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {Promise<void>}
 */
exports.userConnection = async (req, res) => {
  const user = await new User({
    username: req.body.username,
    password: req.body.password,
  });

  // Authentification puis login session
  passport.authenticate("local", function (err, user, info) {
    if (err) {
      console.log(err);
      res.redirect("/auth/login");
    } else if (!user) {
      req.session.errorMessage = "Nom d'utilisateur ou mot de passe incorrect";
      res.redirect("/auth/login");
    } else {
      req.login(user, function (err) {
        if (err) {
          console.log(err);
          req.session.errorMessage = "Erreur lors de la connexion";
          res.redirect("/auth/login");
        } else {
          res.redirect("/");
        }
      });
    }
  })(req, res);
};

// Route : déconnexion

/**
 * The `exports.userDisconnection` function is responsible for handling the logout process of a user. It takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework.
 * 
 * @property
 * @name userDisconnection
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {void}
 */
exports.userDisconnection = (req, res) => {
  // méthode de déconnexion de passport
  req.logout(function (err) {
    if (err) {
      console.log(err);
    }
    res.redirect("/");
  });
};

// Route : affichage du profil de l'utilisateur connecté

/**
 * The `exports.userProfil` function is responsible for rendering the user profile page. It takes in the `req` (request) and `res` (response) parameters, which are provided by the Express.js framework.
 * 
 * @property
 * @name userProfil
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @returns {void}
 */
exports.userProfil = (req, res) => {
  // utilisateur est connecté ?
  if (req.isAuthenticated()) {
    // Si oui, affiche la page de profil
    res.render('auth/show', { user: req.user });
  } else {
    // Si non, redirige vers la page de connexion
    res.redirect('/auth/login');
  }
};