const passport = require("passport");
const User = require("../models/user");

/**
 * The `passport.serializeUser` function is used to define how user information should be serialized and stored in the session. In this case, it takes a callback function with two parameters: `user` and `done`.
 * 
 * @constant
 * @name passport
 * @type {PassportStatic import passport}
 */
passport.serializeUser((user, done) => {
  done(null, user.id);
});

/**
 * The `passport.deserializeUser` function is used to retrieve user information from the session and deserialize it into a user object. In this case, it takes a callback function with two parameters: `id` and `done`.
 * 
 * @constant
 * @name passport
 * @type {PassportStatic import passport}
 */
passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.findById(id);
    done(null, user);
  } catch (err) {
    done(err);
  }
});

/**
 * The `exports.usersList` function is an asynchronous function that handles the logic for retrieving a list of users. It takes in three parameters: `req` (the request object), `res` (the response object), and `next` (the next middleware function).
 * 
 * @async
 * @property
 * @name usersList
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {Promise<void>}
 */
exports.usersList = async (req, res, next) => {
  try {
    const members = await User.find({});

    /*   console.log(res.locals["user"]); */

    const message = {};

    if (res.locals["errorMessage"]) {
      message.errorMessage = res.locals["errorMessage"];
    }

    if (res.locals["successMessage"]) {
      message.successMessage = res.locals["successMessage"];
    }

    res.render("admin/index", { members, message });
  } catch (err) {
    next(err);
  }
};

/**
 * The `exports.userEForm` function is an asynchronous function that handles the logic for rendering the edit form for a specific user. It takes in three parameters: `req` (the request object), `res` (the response object), and `next` (the next middleware function).
 * 
 * @async
 * @property
 * @name userEForm
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {Promise<any>}
 */
exports.userEForm = async (req, res, next) => {
  try {
    const member = await User.findById(req.params.id);
    if (!member) {
      return res.status(404).send("Utilisateur non trouvé");
    }

    res.render("admin/edit", { member });
  } catch (error) {
    next(error);
  }
};

/**
 * The `exports.updateUser` function is an asynchronous function that handles the logic for updating a user's information. It takes in three parameters: `req` (the request object), `res` (the response object), and `next` (the next middleware function).
 * 
 * @async
 * @property
 * @name updateUser
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {Promise<any>}
 */
exports.updateUser = async (req, res, next) => {
  try {
    const memberId = req.params.id;
    const updatedMember = req.body;

    const member = await User.findById(memberId);
    if (!member) {
      req.session.errorMessage = "Utilisateur non trouvé";
      return res.redirect("/admin");
    }

    console.log(typeof updatedMember.role, typeof member.role);

    /*     if (
      member.role <= req.user.role &&
      member._id.toString() !== req.user._id.toString() &&
      updatedMember.role !== member.role
    ) {
      req.session.errorMessage = "Modification de l'utilisateur refusée";
      return res.redirect("/admin");
    } */

    if (updatedMember.username && updatedMember.username !== member.username) {
      const usernameExists = await User.findOne({
        username: updatedMember.username,
      });
      if (usernameExists) {
        req.session.errorMessage = "Ce nom d'utilisateur est déjà utilisé";
        return res.redirect(`/admin/${memberId}/edit`);
      }
      member.username = updatedMember.username;
    }

    if (updatedMember.password) {
      await member.setPassword(updatedMember.password);
    }

    if (
      member.role <= req.user.role &&
      (member._id.toString() !== req.user._id.toString() ||
        member._id.toString() === req.user._id.toString())
    ) {
      if (parseInt(updatedMember.role) !== member.role) {
        req.session.errorMessage =
          "Modification du rôle de l'utilisateur refusée";
      }
    } else {
      member.role = updatedMember.role;
    }

    await member.save();
    if (!req.session.errorMessage) {
      req.session.successMessage = "Les modifications ont été enregistrées";
    }
    res.redirect("/admin");
  } catch (error) {
    next(error);
  }
};

/**
 * The `exports.deleteUser` function is an asynchronous function that handles the logic for deleting a user. It takes in three parameters: `req` (the request object), `res` (the response object), and `next` (the next middleware function).
 * 
 * @async
 * @property
 * @name deleteUser
 * @kind function
 * @type {function}
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns {Promise<any>}
 */
exports.deleteUser = async (req, res, next) => {
  try {
    const member = await User.findById(req.params.id);

    if (!member) {
      req.session.errorMessage = "Utilisateur non trouvé";
      return res.redirect("/admin");
    }

    if (req.user._id.toString() === member._id.toString()) {
      req.session.errorMessage =
        "Vous ne pouvez pas supprimer votre propre compte";
      return res.redirect("/admin");
    }

    if (member.role === 1 || member.role <= req.user.role) {
      req.session.errorMessage =
        "Vous ne pouvez pas supprimer un compte ayant des privilèges égaux ou supérieurs aux votres";
      return res.redirect("/admin");
    }

    await User.findByIdAndDelete(req.params.id);
    req.session.successMessage = "L'utilisateur a été supprimé";
    res.redirect("/admin");
  } catch (error) {
    next(error);
  }
};
