// Imports

const mongoose = require("mongoose");
const path = require('path');

const flyerImageBasePath = 'uploads/sessionFlyers';

//

const sessionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  actionNumber: {
    type: String,
  },
  location: {
    type: String,
  },
  sessionStart: {
    type: Date,
    required: true,
  },
  sessionEnd: {
    type: Date,
    required: true,
  },
  flyer: {
    type: String,
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now,
  },
  formation: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Formation',
  },
});

/**
 * The code `sessionSchema.virtual('flyerPath').get(function() {` is defining a virtual property called `flyerPath` for the `sessionSchema` schema.
 * 
 * @constant
 * @name sessionSchema
 * @type {mongoose.Schema<any, mongoose.Model<any, any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, { title: string; sessionStart: Date; sessionEnd: Date; ... 5 more ...; flyer?: string | undefined; }, mongoose.Document<...> & ... 1 more ... & { ...; }>}
 */
sessionSchema.virtual('flyerPath').get(function() {

  // utilisation d’une fonction normale et non d’une fonction flêchée pour avoir accès à this
  if (this.flyer != null) {
    return path.join('/', flyerImageBasePath, this.flyer);
  }
}); // Propriété virtuelle dépendant d’autres propriétés

module.exports = mongoose.model("Session", sessionSchema);
module.exports.flyerImageBasePath = flyerImageBasePath;
