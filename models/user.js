const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
  },
  role: {
    type: Number,
  },
});

// Hash du mot de passe avec passport-local-mongoose

userSchema.plugin(passportLocalMongoose);

// Export

module.exports = mongoose.model('User', userSchema);

