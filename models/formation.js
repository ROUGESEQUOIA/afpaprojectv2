const mongoose = require("mongoose");
const Session = require("./session");

const formationSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  centerTime: {
    type: Number,
  },
  companyTime: {
    type: Number,
  },
  validationType: {
    type: String,
  },
  actionType: {
    type: String,
  },
  center_id: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

/**
 * The `async function checkSessionsExist(formationId)` is a function that checks if there are any sessions associated with a formation. It takes a `formationId` as a parameter and returns a promise that resolves to a boolean value.
 * 
 * @async
 * @function
 * @name checkSessionsExist
 * @kind function
 * @param {any} formationId
 * @returns {Promise<boolean>}
 */
async function checkSessionsExist(formationId) {
  const sessions = await Session.exists({ formation: formationId });

  return sessions ? false : true;
}

// Middlewares

// pre permet de lancer une fonction avant l’événement passé en premier paramètre

/**
 * `formationSchema.pre(` is a method provided by Mongoose that allows you to define middleware functions that are executed before a specific event occurs on a document. In this case, the middleware is defined to be executed before the `deleteOne` event on a document of the `Formation` model.
 * 
 * @constant
 * @name formationSchema
 * @type {mongoose.Schema<any, mongoose.Model<any, any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, { title: string; createdAt: Date; description?: string | undefined; ... 4 more ...; center_id?: number | undefined; }, mongoose.Document<...> & ... 1 more ... & { ...; }>}
 */
formationSchema.pre(
  "deleteOne",
  { document: true, query: false },
  async function (next) {
    try {
      const sessionsExist = await checkSessionsExist(this._id);
      if (!sessionsExist) {
        const err = new Error(
          "Cette formation contient encore au moins une session"
        );
        err.name = "FormationDeleteError";
        throw err;
      }
      console.log("La suppression est autorisée");
      next();
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

module.exports = mongoose.model("Formation", formationSchema);
