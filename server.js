if (process.env.NODE_ENV !== "production") {
  require("dotenv").config(); // librairie qui permet de stocker des variables locales dans un fichier .env
}

// Imports généraux

const express = require("express"); // permet l’utilisation du framework Express JS
const mongoose = require("mongoose"); // permet la connexion à la bdd MongoDB
const helmet = require("helmet"); // Sécurise les headers http
const mongoSanitize = require("express-mongo-sanitize"); // protège des injections NoSql
const xssProtect = require("xss-clean"); // évite l’injection de scripts XSS (ex html img)
const bodyParser = require("body-parser"); // permet de récupérer simplement les données de formulaires
const expressLayouts = require("express-ejs-layouts"); // permet d’utiliser un template dans lequel on pourra insérer nos views
const methodOverride = require("method-override"); // permet de gérer des méthode supplémentaires "put", "delete"

const session = require("express-session"); // permet de gérer les sessions (durée, ect)
const passport = require("passport"); // système d’authentification

// Imports middlewares

const clearMessages = require("./middlewares/clearMessages");

// Imports routes

const indexRouter = require("./routes/index");
const formationsRouter = require("./routes/formations");
const sessionsRouter = require("./routes/sessions");
const usersRouter = require('./routes/auth');
const adminRouter = require('./routes/admin');

// Config générale

const app = express();

mongoose.connect(process.env.DATABASE_URL);
const db = mongoose.connection; // permet de stocker l’état de la connexion
db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Connected to MongoDB"));

// Sécurité

app.use(helmet());
app.use(mongoSanitize());
app.use(xssProtect());

//

app.use(express.static("public")); // indique où seront les fichiers css et les scripts js
app.use(bodyParser.urlencoded({ extended: false, limit: "10mb" })); // permet de récupérer des valeurs via les url
app.use(expressLayouts);
app.use(methodOverride("_method"));

app.use( // setup session
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize()); // initialisation de passport
app.use(passport.session()) // utilisation de passport avec session

app.use(function(req, res, next) { // stockage de l’user dans l’objet locals de la réponse
  res.locals.user = req.user;
  next();
});

app.set("view engine", "ejs"); // type de fichiers des views
app.set("views", __dirname + "/views"); // path des views
app.set("layout", "layouts/layout"); // path du template des views

// config middlewares

app.use(clearMessages);

// Config routes

app.use("/", indexRouter);
app.use("/formations", formationsRouter);
app.use("/sessions", sessionsRouter);
app.use("/auth", usersRouter);
app.use("/admin", adminRouter);


// gestion des pages inexistantes

app.get('*', (req, res) => {
  res.status(404).redirect("/");
});

// Config port serveur local

app.listen(process.env.PORT || 3000);
